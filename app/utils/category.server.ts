import { prisma } from "./db.server";

export async function createCategory({ nama }: any) {
  try {
    const createdCategory = await prisma.category.create({
      data: {
        nama,
      },
    });
    return createdCategory;
  } catch (error) {
    console.error("Error creating category", error);
    throw error;
  }
}

export async function getAllCategories() {
  try {
    const categories = await prisma.category.findMany();
    return categories;
  } catch (error) {
    console.error("Error fetching all categories", error);
    throw error;
  }
}
