import { createCookieSessionStorage, redirect } from "@remix-run/node";
import bcrypt from "bcryptjs";
import { prisma } from "./db.server";

export async function getUserByUsername(username: string) {
  try {
    const getusername = await prisma.user.findUnique({
      where: {
        username,
      },
    });
    return getusername;
  } catch (error) {
    console.log(error);
  }
}

export async function register({ username, email, pass }: any) {
  const hash = await bcrypt.hash(pass, 10);
  try {
    const regis = await prisma.user.create({
      data: {
        username,
        email,
        pass: hash,
      },
    });
    return regis;
  } catch (error) {
    console.log(error);
  }
}

export async function login(username: any, pass: any) {
  const user = await getUserByUsername(username);

  if (!user) return null;

  const isCorrectPassword = await bcrypt.compare(pass, user.pass);

  if (!isCorrectPassword) return null;

  return user;
}

const sessionSecret = process.env.SESSION_SECRET;
if (!sessionSecret) throw new Error("SESSION_SECRET must be set");

const oneDay = 60 * 60 * 60 * 24;
const { getSession, commitSession, destroySession } =
  createCookieSessionStorage({
    cookie: {
      name: "_session",
      secure: process.env.NODE_ENV === "production",
      httpOnly: true,
      secrets: [sessionSecret],
      maxAge: oneDay,
      path: "/",
      sameSite: "lax",
    },
  });

export async function createUserSession(userId: any) {
  const session = await getSession();
  session.set("userId", userId);
  return redirect("/", {
    headers: {
      "Set-Cookie": await commitSession(session),
    },
  });
}

function getUserSession(request: any) {
  const cookie = request.headers.get("Cookie");
  return getSession(cookie);
}

export async function getUserId(request: any) {
  const userSession = await getUserSession(request);
  if (!userSession.has("userId")) return null;

  const userId = userSession.get("userId");
  return userId;
}

export async function requireSession(request: any) {
  const userId = await getUserId(request);
  if (!userId) {
    // Redirect to login page or handle unauthorized access
    throw redirect("/login"); // You may want to redirect to your login page
  }

  return userId;
}

export async function noRequireSession(request: any) {
  const userId = await getUserId(request);
  if (userId) {
    // Redirect to the dashboard or handle authenticated user access
    throw redirect("/");
  }
}

export async function logout(request: any) {
  const session = await getUserSession(request);
  return redirect("/login", {
    headers: {
      "Set-Cookie": await destroySession(session),
    },
  });
}
