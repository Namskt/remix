import { prisma } from "./db.server";

export async function createArtikel({
  title,
  content,
  imageUrl,
  userId,
  categoryId,
}: any) {
  try {
    const createdArtikel = await prisma.artikel.create({
      data: {
        title,
        content,
        imageUrl,
        userId,
        categoryId,
      },
    });
    return createdArtikel;
  } catch (error) {
    console.error("Error creating artikel", error);
    throw error;
  }
}

export async function getAllArtikel() {
  try {
    const allArtikel = await prisma.artikel.findMany();
    return allArtikel;
  } catch (error) {
    console.error("Error fetching all articles", error);
    throw error;
  }
}

export async function getArtikelByCategory(categoryId: string) {
  try {
    const articlesByCategory = await prisma.artikel.findMany({
      where: {
        categoryId: categoryId,
      },
    });
    return articlesByCategory;
  } catch (error) {
    console.error("Error fetching articles by category", error);
    throw error;
  }
}
