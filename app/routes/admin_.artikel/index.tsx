import { LoaderFunction } from "@remix-run/node";
import { useLoaderData } from "@remix-run/react";
import React from "react";
import Layouts from "~/components/layouts/layouts";

// export const loader: LoaderFunction = async () => {
//   const artikel = await getAllArtikel();
//   return { artikel };
// };

export default function Artikel() {
  const { artikel }: any = useLoaderData();
  return (
    <Layouts>
      <div className="px-7 mt-3">
        <div className="flex justify-between items-center toko px-3">
          <div>
            <h1 className="font-semibold text-2xl">Artikel</h1>
          </div>
          <div className="flex items-center gap-2">
            <h1 className="text-xl font-semibold">Category :</h1>
            <h1 className="text-lg font-medium">All Artikel</h1>
          </div>
        </div>
        <div className=" space-y-5">
          {artikel.map((singleArtikel: any) => {
            return (
              <li key={singleArtikel.id} className="bg-white">
                <h2 className="text-2xl font-semibold">
                  {singleArtikel.title}
                </h2>
                {/* Menampilkan konten artikel */}
                <p className="text-gray-700">{singleArtikel.content}</p>
              </li>
            );
          })}
        </div>
      </div>
    </Layouts>
  );
}
