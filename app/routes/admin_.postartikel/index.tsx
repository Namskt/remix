import { ActionFunction, LoaderFunction, json } from "@remix-run/node";
import { Form, useLoaderData } from "@remix-run/react";
import Layouts from "~/components/layouts/layouts";
import { createArtikel } from "~/utils/artikel.server";
import { getAllCategories } from "~/utils/category.server";
import { getUserId, requireSession } from "~/utils/session.server";

export const loader: LoaderFunction = async ({ request }) => {
  await requireSession(request);
  const categories = await getAllCategories();
  return json({ categories });
};

export const action: ActionFunction = async ({ request }) => {
  const body = new URLSearchParams(await request.text());
  const title = body.get("title");
  const content = body.get("content");
  const category = body.get("category");

  if (!title || !content || !category) {
    return json(
      { error: "Title, content, and category are required" },
      { status: 400 }
    );
  }

  const userId = await getUserId(request);
  try {
    const categoryId = "456"; // Replace this with your logic to get the categoryId

    await createArtikel({
      title,
      content,
      userId,
      categoryId,
    });

    return json({ success: true });
  } catch (error) {
    console.error("Error creating artikel:", error);
    return json({ error: "Failed to create artikel" }, { status: 500 });
  }
};
export default function PostArtikel() {
  const { categories }: any = useLoaderData();

  return (
    <Layouts>
      <div className="mt-3 px-7">
        <div>
          <h1 className="toko text-2xl font-medium">Tambah Artikel</h1>
        </div>
        <div className="mt-3">
          <Form method="POST" className="space-y-3">
            <div className="flex items-center justify-center gap-3">
              <div className="w-full">
                <input
                  type="text"
                  name="title"
                  placeholder="Title"
                  className="p-3 px-5 w-full outline-blue-800"
                />
              </div>
              <div className="w-full">
                <select
                  name="category"
                  className="p-3 px-5 w-full outline-blue-800"
                >
                  <option value="" disabled>
                    Category
                  </option>
                  {categories?.map((category: any) => (
                    <option key={category.id} value={category.nama}>
                      {category.nama}
                    </option>
                  ))}
                </select>
              </div>
            </div>

            <div className="">
              <textarea
                placeholder=""
                name="content"
                className="w-full h-96 outline-none p-4"
              />
            </div>
            <div className="">
              <button
                type="submit"
                className="bg-blue-800 p-3 px-10 text-white"
              >
                Save
              </button>
            </div>
          </Form>
        </div>
      </div>
    </Layouts>
  );
}
